<?php
// Definir o cabeçalho para resposta JSON
global $conn;
header('Content-Type: application/json');

// Incluir a conexão ao banco de dados
include 'banco/conexaoBd.php';

// Obter dados do formulário
$nome = $_POST['nome'];
$email = $_POST['email'];
$telefone = $_POST['telefone'];
$cpf = $_POST['cpf'];
$animal = $_POST['animal'];
$especie = $_POST['especie'];
$raca = $_POST['raca'];
$data = $_POST['data'];
$preco = $_POST['preco'];
$mensagem = $_POST['mensagem'];

// Converte a data e hora para o formato YYYY-MM-DD HH:MM:SS
$datetime_obj = DateTime::createFromFormat('d/m/Y H:i', $data);
$formatted_datetime = $datetime_obj->format('Y-m-d H:i:s');

// Verificar se o cliente já existe pelo CPF
$sql = "SELECT id_cliente FROM tb_cliente WHERE CPF = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("s", $cpf);
$stmt->execute();
$result = $stmt->get_result();
if ($result->num_rows > 0) {
    // Cliente já existe, obter o ID do cliente
    $row = $result->fetch_assoc();
    $cliente_id = $row['id_cliente'];
} else {
    // Inserir novo cliente
    $sql = "INSERT INTO tb_cliente (CPF, nome, telefone, email) VALUES (?, ?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssss", $cpf, $nome, $telefone, $email);
    if (!$stmt->execute()) {
        echo json_encode(['success' => false, 'message' => 'Erro ao inserir cliente: ' . $stmt->error]);
        exit;
    }
    $cliente_id = $stmt->insert_id;
}

// Inserir novo animal
$sql = "INSERT INTO tb_animal (nome, especie, raca, cliente_id) VALUES (?, ?, ?, ?)";
$stmt = $conn->prepare($sql);
$stmt->bind_param("sssi", $animal, $especie, $raca, $cliente_id);
if (!$stmt->execute()) {
    echo json_encode(['success' => false, 'message' => 'Erro ao inserir animal: ' . $stmt->error]);
    exit;
}
$animal_id = $stmt->insert_id;

// Inserir agendamento
$sql = "INSERT INTO tb_agendamento (_data, descricao_servico, preco, animal_id, id_cliente) VALUES (?, ?, ?, ?, ?)";
$stmt = $conn->prepare($sql);
$stmt->bind_param("ssdii", $formatted_datetime, $mensagem, $preco, $animal_id, $cliente_id);
if ($stmt->execute()) {
    echo json_encode(['success' => true, 'message' => 'Agendamento realizado com sucesso!']);
} else {
    echo json_encode(['success' => false, 'message' => 'Erro ao inserir agendamento: ' . $stmt->error]);
}

// Fechar a conexão
$stmt->close();
$conn->close();
?>
