<?php
global $conn;
header('Content-Type: application/json');

if (isset($_GET['id'])) {
    $id = intval($_GET['id']); // Sanitiza o ID

    // Incluir a conexão ao banco de dados
    include 'banco/conexaoBd.php';

    // Verificar a conexão
    if ($conn->connect_error) {
        echo json_encode(['success' => false, 'message' => "Falha na conexão: " . $conn->connect_error]);
        exit();
    }

    // Buscar o agendamento juntamente com os dados do cliente e do animal
    $sql = "SELECT 
                ag.id_agendamento, ag._data, ag.preco, ag.descricao_servico,
                c.nome AS cliente_nome, c.email, c.telefone, c.cpf,
                a.nome AS animal_nome, a.especie, a.raca
            FROM tb_agendamento ag
            JOIN tb_animal a ON ag.animal_id = a.id_animal
            JOIN tb_cliente c ON a.cliente_id = c.id_cliente
            WHERE ag.id_agendamento = ?";

    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $agendamento = $result->fetch_assoc();
        echo json_encode(['success' => true, 'agendamento' => $agendamento]);
    } else {
        echo json_encode(['success' => false, 'message' => "Agendamento não encontrado"]);
    }

    $stmt->close();
    $conn->close();
} else {
    echo json_encode(['success' => false, 'message' => "ID do agendamento não fornecido"]);
}
?>