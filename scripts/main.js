document.addEventListener("DOMContentLoaded", function () {
    document.getElementById('agendamentoForm').addEventListener('submit', function (event) {
        event.preventDefault();

        var formData = new FormData(this);

        fetch('agendar_consulta.php', {
            method: 'POST',
            body: formData
        })
            .then(response => response.json())
            .then(data => {
                console.log("Dados Salvar: ", formData);
                displayMessage(data.message, data.success);
                if (data.success) {
                    clearForm();
                    loadAgendamentos();
                }
            })
            .catch(error => {
                displayMessage('Erro ao realizar agendamento. Por favor, tente novamente.' + error, false);
            });
    });

    function displayMessage(message, success) {
        alert(message);
    }

    function clearForm() {
        document.getElementById('agendamentoForm').reset();
    }

    function loadAgendamentos() {
        fetch('listar_agendamentos.php')
            .then(response => response.text())
            .then(data => {
                document.getElementById('agendamentos').innerHTML = data;
            });
    }

    function deletarAgendamento(id) {
        if (confirm('Você tem certeza que deseja deletar este agendamento?')) {
            fetch('deletar_agendamento.php?id=' + id, {
                method: 'GET'
            })
                .then(response => response.json())
                .then(data => {
                    displayMessage(data.message, data.success);
                    if (data.success) {
                        loadAgendamentos();
                    }
                })
                .catch(error => {
                    displayMessage('Erro ao deletar agendamento. Por favor, tente novamente.' + error, false);
                });
        }
    }

    window.deletarAgendamento = deletarAgendamento;

    loadAgendamentos();
});
