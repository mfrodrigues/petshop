function formatarData(dataString) {
    // Extrair partes da data
    let partes = dataString.split(' ')[0].split('-');

    // Montar data no formato dd/mm/yyyy
    return partes[2] + '/' + partes[1] + '/' + partes[0];
}

function formatarDataCompleta(dataString) {
    console.log("Data chegou: ", dataString);
    // Converter a string de data para um objeto Date
    let data = new Date(dataString);

    // Extrair as partes da data
    let dia = data.getDate().toString().padStart(2, '0');
    let mes = (data.getMonth() + 1).toString().padStart(2, '0');
    let ano = data.getFullYear();
    let hora = data.getHours().toString().padStart(2, '0');
    let minuto = data.getMinutes().toString().padStart(2, '0');

    // Montar a data no formato desejado
    let dataFormatada = dia + '/' + mes + '/' + ano + ' ' + hora + ':' + minuto;
    console.log("Retornar: ", dataFormatada);
    return dataFormatada;
}