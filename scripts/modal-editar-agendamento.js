document.addEventListener("DOMContentLoaded", function () {
    function openEditModal(id) {
        fetch('buscar_agendamento.php?id=' + id)
            .then(response => response.json())
            .then(data => {
                if (data.success) {
                    document.getElementById('edit-id_agendamento').value = data.agendamento.id_agendamento;
                    document.getElementById('edit-nome').value = data.agendamento.cliente_nome;
                    document.getElementById('edit-email').value = data.agendamento.email;
                    document.getElementById('edit-telefone').value = data.agendamento.telefone;
                    document.getElementById('edit-cpf').value = data.agendamento.cpf;
                    document.getElementById('edit-animal').value = data.agendamento.animal_nome;
                    document.getElementById('edit-especie').value = data.agendamento.especie;
                    document.getElementById('edit-raca').value = data.agendamento.raca;
                    document.getElementById('edit-data').value = data.agendamento._data;
                    document.getElementById('edit-preco').value = data.agendamento.preco;
                    document.getElementById('edit-mensagem').value = data.agendamento.descricao_servico;
                    document.getElementById('modalEditar').style.display = 'block';
                } else {
                    displayMessage(data.message, false);
                }
            })
            .catch(error => {
                displayMessage('Erro ao carregar agendamento para edição. Por favor, tente novamente. ' + error, false);
            });
    }

    function closeEditModal() {
        document.getElementById('modalEditar').style.display = 'none';
    }

    // Fechar a modal ao clicar no botão de fechar
    document.getElementsByClassName('close')[0].addEventListener('click', function () {
        closeEditModal();
    });

    // Fechar a modal se clicar fora dela (na área escura)
    window.addEventListener('click', function (event) {
        if (event.target === document.getElementById('modalEditar')) {
            closeEditModal();
        }
    });

    // Função para enviar o formulário de edição
    document.getElementById('editAgendamentoForm').addEventListener('submit', function (event) {
        event.preventDefault();

        var formData = new FormData(this);

        fetch('editar-agendamento.php', {
            method: 'POST',
            body: formData
        })
            .then(response => response.json())
            .then(data => {
                displayMessage(data.message, data.success);
                if (data.success) {
                    closeEditModal();
                    loadAgendamentos();
                }
            })
            .catch(error => {
                displayMessage('Erro ao atualizar agendamento. Por favor, tente novamente. ' + error, false);
            });
    });

    // Função para exibir mensagens de alerta
    function displayMessage(message, success) {
        alert(message);
    }

    // Atribuir função de abrir a modal de edição ao escopo global
    window.openEditModal = openEditModal;

    // Carregar os agendamentos ao carregar a página
    loadAgendamentos();

    function loadAgendamentos() {
        fetch('listar_agendamentos.php')
            .then(response => response.text())
            .then(data => {
                document.getElementById('agendamentos').innerHTML = data;
            });
    }

});

