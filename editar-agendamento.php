<?php
// Verificar o método de requisição
global $conn;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obter dados do formulário
    $id_agendamento = $_POST['id_agendamento'];
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $telefone = $_POST['telefone'];
    $cpf = $_POST['cpf'];
    $animal = $_POST['animal'];
    $especie = $_POST['especie'];
    $raca = $_POST['raca'];
    $data = $_POST['data'];
    $preco = $_POST['preco'];
    $mensagem = $_POST['mensagem'];

    // Converte a data e hora para o formato YYYY-MM-DD HH:MM:SS
    $datetime_obj = DateTime::createFromFormat('d/m/Y H:i', $data);
    $formatted_datetime = $datetime_obj->format('Y-m-d H:i:s');
    // Conectar ao banco de dados
    include 'banco/conexaoBd.php'; // Verifique se este arquivo contém a conexão correta ao banco de dados

    // Verificar a conexão
    if ($conn->connect_error) {
        die(json_encode(['success' => false, 'message' => "Falha na conexão: " . $conn->connect_error]));
    }

    // Iniciar transação
    $conn->begin_transaction();

    try {
        // Recuperar o agendamento para obter id_cliente e id_animal
        $sql = "SELECT id_cliente, animal_id FROM tb_agendamento WHERE id_agendamento = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $id_agendamento);
        $stmt->execute();
        $stmt->bind_result($id_cliente, $id_animal);
        $stmt->fetch();
        $stmt->close();

        if (empty($id_cliente) || empty($id_animal)) {
            throw new Exception("Agendamento não encontrado ou dados incompletos.");
        }

        // Atualizar o cliente
        $sql = "UPDATE tb_cliente SET 
            nome=?, 
            email=?, 
            telefone=?, 
            cpf=? 
        WHERE id_cliente=?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("ssssi", $nome, $email, $telefone, $cpf, $id_cliente);
        if (!$stmt->execute()) {
            throw new Exception("Erro ao atualizar cliente: " . $stmt->error);
        }
        $stmt->close();

        // Atualizar o animal
        $sql = "UPDATE tb_animal SET 
            nome=?, 
            especie=?, 
            raca=? 
        WHERE id_animal=?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sssi", $animal, $especie, $raca, $id_animal);
        if (!$stmt->execute()) {
            throw new Exception("Erro ao atualizar animal: " . $stmt->error);
        }
        $stmt->close();

        // Atualizar o agendamento
        $sql = "UPDATE tb_agendamento SET 
            _data=?, 
            preco=?, 
            descricao_servico=?
        WHERE id_agendamento=?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sssi", $formatted_datetime, $preco, $mensagem, $id_agendamento);
        if (!$stmt->execute()) {
            throw new Exception("Erro ao atualizar agendamento: " . $stmt->error);
        }
        $stmt->close();

        // Commit da transação
        $conn->commit();

        echo json_encode(['success' => true, 'message' => "Agendamento atualizado com sucesso"]);
    } catch (Exception $e) {
        // Rollback da transação em caso de erro
        $conn->rollback();
        echo json_encode(['success' => false, 'message' => "Erro ao atualizar agendamento: " . $e->getMessage()]);
    }

    // Fechar a conexão
    $conn->close();
} else {
    echo json_encode(['success' => false, 'message' => "Método de requisição inválido"]);
}
?>
