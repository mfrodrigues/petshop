<?php
// Incluir a conexão ao banco de dados
global $conn;
include 'banco/conexaoBd.php';

// Buscar agendamentos do banco de dados
$sql = "SELECT ag.id_agendamento, c.nome, c.email, c.telefone, c.CPF, a.nome AS animal, a.especie, a.raca, ag._data, ag.preco, ag.descricao_servico
        FROM tb_agendamento ag
        JOIN tb_animal a ON ag.animal_id = a.id_animal
        JOIN tb_cliente c ON a.cliente_id = c.id_cliente";
$result = $conn->query($sql);

// Exibir os agendamentos na tabela
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $data_formatada = date('d/m/Y H:i', strtotime($row['_data']));
        echo "<tr>
                <td>{$row['nome']}</td>
                <td>{$row['email']}</td>
                <td>{$row['telefone']}</td>
                <td>{$row['CPF']}</td>
                <td>{$row['animal']}</td>
                <td>{$row['especie']}</td>
                <td>{$row['raca']}</td>
                <td>{$data_formatada}</td>
                <td> R$ {$row['preco']}</td>
                <td>{$row['descricao_servico']}</td>
                <td><button onclick=\"openEditModal({$row['id_agendamento']})\">Editar</button> <button onclick=\"deletarAgendamento({$row['id_agendamento']})\">Deletar</button></td>
            </tr>";
    }
} else {
    echo "<tr><td colspan='11'>Nenhum agendamento encontrado</td></tr>";
}

// Fechar a conexão
$conn->close();
?>
