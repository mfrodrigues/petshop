<?php
// Configurações do banco de dados
$servername = "localhost";
$username = "root";
$password = "root123";
$dbname = "db_petshop";

// Conectar ao banco de dados
$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar conexão
if ($conn->connect_error) {
    die("Falha na conexão: " . $conn->connect_error);
}
?>
