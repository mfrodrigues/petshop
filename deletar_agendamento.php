<?php
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Incluir a conexão ao banco de dados
    global $conn;
    include 'banco/conexaoBd.php';

    // Verificar a conexão
    if ($conn->connect_error) {
        die(json_encode(['success' => false, 'message' => "Falha na conexão: " . $conn->connect_error]));
    }

    // Deletar o agendamento
    $sql = "DELETE FROM tb_agendamento WHERE id_agendamento = $id";
    if ($conn->query($sql) === TRUE) {
        echo json_encode(['success' => true, 'message' => "Agendamento deletado com sucesso"]);
    } else {
        echo json_encode(['success' => false, 'message' => "Erro ao deletar agendamento: " . $conn->error]);
    }

    $conn->close();
} else {
    echo json_encode(['success' => false, 'message' => "ID do agendamento não fornecido"]);
}
?>
